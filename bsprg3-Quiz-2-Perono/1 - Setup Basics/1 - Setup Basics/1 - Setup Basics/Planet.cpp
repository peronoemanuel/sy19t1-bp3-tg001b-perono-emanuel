#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include "BaseApplication.h"
#include "Planet.h"

Planet::Planet(SceneNode *node)
{
	mNode = node;
}
Planet::~Planet()
{
}
void Planet::createPlanet(void)
{
	ManualObject *object = mSceneMgr->createManualObject();
	ManualObject *mercury = mSceneMgr->createManualObject();
	ManualObject *venus = mSceneMgr->createManualObject();
	ManualObject *earth = mSceneMgr->createManualObject();
	ManualObject *moon = mSceneMgr->createManualObject();
	ManualObject *mars = mSceneMgr->createManualObject();
	//Begin drawing
	object->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	mercury->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	venus->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	earth->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	moon->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	mars->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);

	int size = 20;

	//-------------------------------------
	//FRONT
	object->colour(ColourValue::White);
	object->position(size, -size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, -size, size); object->normal(Vector3(1, 0, 0));

	object->position(size, -size, size); object->normal(Vector3(1, 0, 0));
	object->position(size, size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, size, size); object->normal(Vector3(1, 0, 0));

	//object->index(0);
	//object->index(1);
	//object->index(2);
	//object->index(2);
	//object->index(3);
	//object->index(0);


	//-------------------------------------
	//LEFT
	object->position(-size, size, -size);  object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, size);	object->normal(Vector3(0, 0, 1));

	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, size, size); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//RIGHT
	object->position(size, -size, size);  object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, size);	object->normal(Vector3(0, 0, 1));

	object->position(size, -size, size);	object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//Back
	object->position(size, size, -size);  object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));

	object->position(size, -size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//TOP
	object->position(-size, size, -size);
	object->position(-size, size, size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, size);	object->normal(Vector3(0, 0, 1));

	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(size, size, size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//BOTTOM
	object->position(size, -size, size);
	object->position(-size, -size, size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));

	object->position(size, -size, size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size); object->normal(Vector3(0, 0, 1));
	//*/
	int Vsize = 6;
	int x = 3 + 58;
	int z = 3;
	int y = 3;

	//x = 20 * cos(45) + 1 * sin(45);
	//z = 5 * -sin(45) + 1 * cos(45);

	//MERCURY
	//-------------------------------------
	//FRONT
	mercury->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	mercury->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x, y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	mercury->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	mercury->position(x, -y, z); object->colour(0, 0, 1);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	mercury->position(x, y, z);	object->colour(0, 1, 0);

	mercury->position(x, -y, z);	object->colour(0, 0, 1);
	mercury->position(x, -y, -z); object->colour(0, 1, 0);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	mercury->position(x, y, -z); object->colour(0, 0, 1);
	mercury->position(x, -y, -z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	mercury->position(x, -y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	mercury->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, y, z); object->colour(1, 0, 0);
	mercury->position(x, y, z);	object->colour(0, 1, 0);

	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x, y, z); object->colour(0, 1, 0);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	mercury->position(x, -y, z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	mercury->position(x, -y, z); object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	mercury->position(x, -y, -z); object->colour(1, 0, 0);

	Vsize = 10;
	x = 5 + 108;
	z = 5;
	y = 5;
	//VENUS
	//-------------------------------------
	//FRONT
	venus->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	venus->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	venus->position(x, y, z); object->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	venus->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	venus->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	venus->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	venus->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	venus->position(x, -y, z); object->colour(0, 0, 1);
	venus->position(x, y, -z); object->colour(1, 0, 0);
	venus->position(x, y, z);	object->colour(0, 1, 0);

	venus->position(x, -y, z);	object->colour(0, 0, 1);
	venus->position(x, -y, -z); object->colour(0, 1, 0);
	venus->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	venus->position(x, y, -z); object->colour(0, 0, 1);
	venus->position(x, -y, -z); object->colour(1, 0, 0);
	venus->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	venus->position(x, -y, -z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	venus->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	venus->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, y, z); object->colour(1, 0, 0);
	venus->position(x, y, z);	object->colour(0, 1, 0);

	venus->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	venus->position(x, y, z); object->colour(0, 1, 0);
	venus->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	venus->position(x, -y, z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	venus->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	venus->position(x, -y, z); object->colour(0, 0, 1);
	venus->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	venus->position(x, -y, -z); object->colour(1, 0, 0);

	Vsize = 20;
	x = 10 + 150;
	z = 10;
	y = 10;
	//EARTH
	//-------------------------------------
	//FRONT
	earth->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	earth->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x, y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	earth->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	earth->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	earth->position(x, -y, z); object->colour(0, 0, 1);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	earth->position(x, y, z);	object->colour(0, 1, 0);

	earth->position(x, -y, z);	object->colour(0, 0, 1);
	earth->position(x, -y, -z); object->colour(0, 1, 0);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	earth->position(x, y, -z); object->colour(0, 0, 1);
	earth->position(x, -y, -z); object->colour(1, 0, 0);
	earth->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	earth->position(x, -y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	earth->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, y, z); object->colour(1, 0, 0);
	earth->position(x, y, z);	object->colour(0, 1, 0);

	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x, y, z); object->colour(0, 1, 0);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	earth->position(x, -y, z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	earth->position(x, -y, z); object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	earth->position(x, -y, -z); object->colour(1, 0, 0);

	Vsize = 2;
	x = 1 + 170;
	z = 1;
	y = 1;
	//MOON
	//-------------------------------------
	//FRONT
	object->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	object->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	object->position(x, y, z); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	object->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	object->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	object->position(x, -y, z); object->colour(0, 0, 1);
	object->position(x, y, -z); object->colour(1, 0, 0);
	object->position(x, y, z);	object->colour(0, 1, 0);

	object->position(x, -y, z);	object->colour(0, 0, 1);
	object->position(x, -y, -z); object->colour(0, 1, 0);
	object->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	object->position(x, y, -z); object->colour(0, 0, 1);
	object->position(x, -y, -z); object->colour(1, 0, 0);
	object->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	object->position(x, -y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	object->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, y, z); object->colour(1, 0, 0);
	object->position(x, y, z);	object->colour(0, 1, 0);

	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x, y, z); object->colour(0, 1, 0);
	object->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	object->position(x, -y, z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	object->position(x, -y, z); object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	object->position(x, -y, -z); object->colour(1, 0, 0);

	Vsize = 16;
	x = 8 + 228;
	z = 8;
	y = 8;
	//MARS
	//-------------------------------------
	//FRONT
	object->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	object->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	object->position(x, y, z); object->normal(Vector3(0, 0, 1));
	object->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	object->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	object->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	object->position(x, -y, z); object->colour(0, 0, 1);
	object->position(x, y, -z); object->colour(1, 0, 0);
	object->position(x, y, z);	object->colour(0, 1, 0);

	object->position(x, -y, z);	object->colour(0, 0, 1);
	object->position(x, -y, -z); object->colour(0, 1, 0);
	object->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	object->position(x, y, -z); object->colour(0, 0, 1);
	object->position(x, -y, -z); object->colour(1, 0, 0);
	object->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	object->position(x, -y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	object->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x - Vsize, y, z); object->colour(1, 0, 0);
	object->position(x, y, z);	object->colour(0, 1, 0);

	object->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	object->position(x, y, z); object->colour(0, 1, 0);
	object->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	object->position(x, -y, z);	object->colour(0, 0, 1);
	object->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	object->position(x, -y, z); object->colour(0, 0, 1);
	object->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	object->position(x, -y, -z); object->colour(1, 0, 0);

	//End drawing
	object->end();
	mercury->end();
	venus->end();
	earth->end();
	moon->end();
	mars->end();
	//Add manual object to the scene
	nodeSun = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeSun->attachObject(object);
	nodeMercury = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMercury->attachObject(mercury);
	nodeVenus = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeVenus->attachObject(venus);
	nodeEarth = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeEarth->attachObject(earth);
	nodeMoon = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMoon->attachObject(moon);
	nodeMars = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMars->attachObject(mars);
	mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5f));

	Light *light = mSceneMgr->createLight();
	light->setType(Light::LightTypes::LT_POINT);
	light->setDiffuseColour(ColourValue(1.0f, 0.7f, 0));
	light->setAttenuation(325, 0.0f, 0.014, 0.0007);
	light->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	light->setPosition(Vector3(0, 0, 0));
	//light->setCastShadows(true);
	//Move
	//node->translate(20, 0, 0);
}

void Planet::update(const FrameEvent & evt)
{


}
SceneNode * Planet::getNode()
{
	return mNode;
}

void Planet::setParent(Planet *parent)
{
}

Planet *Planet::getParent()
{

	return mParent;


}
void Planet::setLocalRotationSpeed(float speed)
{



}
void Planet::setRevolutionSpeed(float speed)
{



}
//Sorry sir if it's copied and paste. I just can't understand this ogre codes. 
//Been finding reference and solution for this planet.h but its too confusing and hard to understand...
//I'm dead tired coding this for daysss just to be stressful again.
//Wish programming subjects is not included in our curriculum taking up art majors like me since
//we are not gonna code when were in a gaming company...T-T