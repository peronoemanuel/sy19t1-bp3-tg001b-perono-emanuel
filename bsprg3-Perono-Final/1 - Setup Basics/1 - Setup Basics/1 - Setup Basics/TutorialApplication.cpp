/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"
//using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	////Begin drawing
	//object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);


	ManualObject *object = mSceneMgr->createManualObject();
	ManualObject *mercury = mSceneMgr->createManualObject();
	ManualObject *venus = mSceneMgr->createManualObject();
	ManualObject *earth = mSceneMgr->createManualObject();
	ManualObject *moon = mSceneMgr->createManualObject();
	ManualObject *mars = mSceneMgr->createManualObject();
	//Begin drawing
	object->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	mercury->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	venus->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	earth->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	moon->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	mars->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);

	int size = 20;
	//Draw your object here
	//object->position(-(size / 2), -(size / 2), size / 2);
	//object->colour(ColourValue::White);
	//object->normal(Vector3(0, 0, 1));
	//object->position((size / 2), -(size / 2), size / 2);
	//object->normal(Vector3(0, 0, 1));
	//object->position((size / 2), -(size / 2), size / 2);
	//object->normal(Vector3(0, 0, 1));

	//-------------------------------------
	//FRONT
	object->colour(ColourValue::White);
	object->position(size, -size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, -size, size); object->normal(Vector3(1, 0, 0));

	object->position(size, -size, size); object->normal(Vector3(1, 0, 0));
	object->position(size, size, size); object->normal(Vector3(1, 0, 0));
	object->position(-size, size, size); object->normal(Vector3(1, 0, 0));

	//object->index(0);
	//object->index(1);
	//object->index(2);
	//object->index(2);
	//object->index(3);
	//object->index(0);


	//-------------------------------------
	//LEFT
	object->position(-size, size, -size);  object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, size);	object->normal(Vector3(0, 0, 1));

	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, size, size); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//RIGHT
	object->position(size, -size, size);  object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, size);	object->normal(Vector3(0, 0, 1));

	object->position(size, -size, size);	object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//Back
	object->position(size, size, -size);  object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));

	object->position(size, -size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(-size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//TOP
	object->position(-size, size, -size);
	object->position(-size, size, size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, size);	object->normal(Vector3(0, 0, 1));

	object->position(-size, size, -size);	object->normal(Vector3(0, 0, 1));
	object->position(size, size, size); object->normal(Vector3(0, 0, 1));
	object->position(size, size, -size); object->normal(Vector3(0, 0, 1));
	////-------------------------------------
	//BOTTOM
	object->position(size, -size, size);
	object->position(-size, -size, size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));

	object->position(size, -size, size); object->normal(Vector3(0, 0, 1));
	object->position(-size, -size, -size); object->normal(Vector3(0, 0, 1));
	object->position(size, -size, -size); object->normal(Vector3(0, 0, 1));
	//*/

	int Vsize = 6;
	int x = 3 + 58;
	int z = 3;
	int y = 3;



	//x = 20 * cos(45) + 1 * sin(45);
	//z = 5 * -sin(45) + 1 * cos(45);


	//MERCURY
	//-------------------------------------
	//FRONT
	mercury->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	mercury->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x, y, z); object->normal(Vector3(0, 0, 1));
	mercury->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	mercury->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	mercury->position(x, -y, z); object->colour(0, 0, 1);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	mercury->position(x, y, z);	object->colour(0, 1, 0);

	mercury->position(x, -y, z);	object->colour(0, 0, 1);
	mercury->position(x, -y, -z); object->colour(0, 1, 0);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	mercury->position(x, y, -z); object->colour(0, 0, 1);
	mercury->position(x, -y, -z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	mercury->position(x, -y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	mercury->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, y, z); object->colour(1, 0, 0);
	mercury->position(x, y, z);	object->colour(0, 1, 0);

	mercury->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	mercury->position(x, y, z); object->colour(0, 1, 0);
	mercury->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	mercury->position(x, -y, z);	object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	mercury->position(x, -y, z); object->colour(0, 0, 1);
	mercury->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	mercury->position(x, -y, -z); object->colour(1, 0, 0);


	Vsize = 10;
	x = 5 + 108;
	z = 5;
	y = 5;

	//VENUS
	//-------------------------------------
	//FRONT
	venus->position(x, -y, z); venus->colour(ColourValue::White); venus->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, y, z); venus->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, -y, z); venus->normal(Vector3(0, 0, 1));

	venus->position(x, -y, z); venus->normal(Vector3(0, 0, 1));
	venus->position(x, y, z); venus->normal(Vector3(0, 0, 1));
	venus->position(x - Vsize, y, z); venus->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	venus->position(x - Vsize, y, -z);	venus->colour(1, 10, 1);
	venus->position(x - Vsize, -y, -z); venus->colour(1, 0, 0);
	venus->position(x - Vsize, -y, z);	venus->colour(0, 1, 0);

	venus->position(x - Vsize, y, -z);	venus->colour(0, 0, 1);
	venus->position(x - Vsize, -y, z);	venus->colour(0, 1, 0);
	venus->position(x - Vsize, y, z); venus->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	venus->position(x, -y, z); venus->colour(0, 0, 1);
	venus->position(x, y, -z); venus->colour(1, 0, 0);
	venus->position(x, y, z);	venus->colour(0, 1, 0);

	venus->position(x, -y, z);	venus->colour(0, 0, 1);
	venus->position(x, -y, -z); venus->colour(0, 1, 0);
	venus->position(x, y, -z); venus->colour(1, 0, 0);
	////-------------------------------------
	//Back
	venus->position(x, y, -z); venus->colour(0, 0, 1);
	venus->position(x, -y, -z); venus->colour(1, 0, 0);
	venus->position(x - Vsize, y, -z);	venus->colour(0, 1, 0);

	venus->position(x, -y, -z);	venus->colour(0, 0, 1);
	venus->position(x - Vsize, -y, -z); venus->colour(0, 1, 0);
	venus->position(x - Vsize, y, -z); venus->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	venus->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	venus->position(x - Vsize, y, z); object->colour(1, 0, 0);
	venus->position(x, y, z);	object->colour(0, 1, 0);

	venus->position(x - Vsize, y, -z);	venus->colour(0, 0, 1);
	venus->position(x, y, z); venus->colour(0, 1, 0);
	venus->position(x, y, -z); venus->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	venus->position(x, -y, z);	venus->colour(0, 0, 1);
	venus->position(x - Vsize, -y, z); venus->colour(1, 0, 0);
	venus->position(x - Vsize, -y, -z); venus->colour(0, 1, 0);

	venus->position(x, -y, z); venus->colour(0, 0, 1);
	venus->position(x - Vsize, -y, -z); venus->colour(0, 1, 0);
	venus->position(x, -y, -z); venus->colour(1, 0, 0);


	Vsize = 20;
	x = 10 + 150;
	z = 10;
	y = 10;

	//EARTH
	//-------------------------------------
	//FRONT
	earth->position(x, -y, z); object->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, -y, z); object->normal(Vector3(0, 0, 1));

	earth->position(x, -y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x, y, z); object->normal(Vector3(0, 0, 1));
	earth->position(x - Vsize, y, z); object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(1, 0, 0);
	earth->position(x - Vsize, -y, z);	object->colour(0, 1, 0);

	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, z);	object->colour(0, 1, 0);
	earth->position(x - Vsize, y, z); object->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	earth->position(x, -y, z); object->colour(0, 0, 1);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	earth->position(x, y, z);	object->colour(0, 1, 0);

	earth->position(x, -y, z);	object->colour(0, 0, 1);
	earth->position(x, -y, -z); object->colour(0, 1, 0);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//Back
	earth->position(x, y, -z); object->colour(0, 0, 1);
	earth->position(x, -y, -z); object->colour(1, 0, 0);
	earth->position(x - Vsize, y, -z);	object->colour(0, 1, 0);

	earth->position(x, -y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	earth->position(x - Vsize, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, y, z); object->colour(1, 0, 0);
	earth->position(x, y, z);	object->colour(0, 1, 0);

	earth->position(x - Vsize, y, -z);	object->colour(0, 0, 1);
	earth->position(x, y, z); object->colour(0, 1, 0);
	earth->position(x, y, -z); object->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	earth->position(x, -y, z);	object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, z); object->colour(1, 0, 0);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);

	earth->position(x, -y, z); object->colour(0, 0, 1);
	earth->position(x - Vsize, -y, -z); object->colour(0, 1, 0);
	earth->position(x, -y, -z); object->colour(1, 0, 0);



	Vsize = 2;
	x = 1 + 170;
	z = 1;
	y = 1;

	//x = 50 * cos(45 ) + 20 * sin(45 );
	//z = 50 * -sin(45 ) + 20 * cos(45);


	//MOON
	//-------------------------------------
	//FRONT
	moon->position(x, -y, z); moon->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	moon->position(x - Vsize, y, z); moon->normal(Vector3(0, 0, 1));
	moon->position(x - Vsize, -y, z); moon->normal(Vector3(0, 0, 1));

	moon->position(x, -y, z); moon->normal(Vector3(0, 0, 1));
	moon->position(x, y, z); moon->normal(Vector3(0, 0, 1));
	moon->position(x - Vsize, y, z); moon->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	moon->position(x - Vsize, y, -z);	moon->colour(0, 0, 1);
	moon->position(x - Vsize, -y, -z); moon->colour(1, 0, 0);
	moon->position(x - Vsize, -y, z);	moon->colour(0, 1, 0);

	moon->position(x - Vsize, y, -z);	moon->colour(0, 0, 1);
	moon->position(x - Vsize, -y, z);	moon->colour(0, 1, 0);
	moon->position(x - Vsize, y, z); moon->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	moon->position(x, -y, z); moon->colour(0, 0, 1);
	moon->position(x, y, -z); moon->colour(1, 0, 0);
	moon->position(x, y, z);	moon->colour(0, 1, 0);

	moon->position(x, -y, z);	moon->colour(0, 0, 1);
	moon->position(x, -y, -z); moon->colour(0, 1, 0);
	moon->position(x, y, -z); moon->colour(1, 0, 0);
	////-------------------------------------
	//Back
	moon->position(x, y, -z); moon->colour(0, 0, 1);
	moon->position(x, -y, -z); moon->colour(1, 0, 0);
	moon->position(x - Vsize, y, -z);	moon->colour(0, 1, 0);

	moon->position(x, -y, -z);	moon->colour(0, 0, 1);
	moon->position(x - Vsize, -y, -z); moon->colour(0, 1, 0);
	moon->position(x - Vsize, y, -z); moon->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	moon->position(x - Vsize, y, -z);	moon->colour(0, 0, 1);
	moon->position(x - Vsize, y, z); moon->colour(1, 0, 0);
	moon->position(x, y, z);	moon->colour(0, 1, 0);

	moon->position(x - Vsize, y, -z);	moon->colour(0, 0, 1);
	moon->position(x, y, z); moon->colour(0, 1, 0);
	moon->position(x, y, -z); moon->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	moon->position(x, -y, z);	moon->colour(0, 0, 1);
	moon->position(x - Vsize, -y, z); moon->colour(1, 0, 0);
	moon->position(x - Vsize, -y, -z); moon->colour(0, 1, 0);

	moon->position(x, -y, z); moon->colour(0, 0, 1);
	moon->position(x - Vsize, -y, -z); moon->colour(0, 1, 0);
	moon->position(x, -y, -z); moon->colour(1, 0, 0);



	Vsize = 16;
	x = 8 + 228;
	z = 8;
	y = 8;

	//MARS
	//-------------------------------------
	//FRONT
	mars->position(x, -y, z); mars->colour(ColourValue::White); object->normal(Vector3(0, 0, 1));
	mars->position(x - Vsize, y, z); mars->normal(Vector3(0, 0, 1));
	mars->position(x - Vsize, -y, z); mars->normal(Vector3(0, 0, 1));

	mars->position(x, -y, z); mars->normal(Vector3(0, 0, 1));
	mars->position(x, y, z); mars->normal(Vector3(0, 0, 1));
	mars->position(x - Vsize, y, z); mars->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	mars->position(x - Vsize, y, -z);	mars->colour(0, 0, 1);
	mars->position(x - Vsize, -y, -z); mars->colour(1, 0, 0);
	mars->position(x - Vsize, -y, z);	mars->colour(0, 1, 0);

	mars->position(x - Vsize, y, -z);	mars->colour(0, 0, 1);
	mars->position(x - Vsize, -y, z);	mars->colour(0, 1, 0);
	mars->position(x - Vsize, y, z); mars->colour(1, 0, 0);
	//-------------------------------------
	//RIGHT
	mars->position(x, -y, z); mars->colour(0, 0, 1);
	mars->position(x, y, -z); mars->colour(1, 0, 0);
	mars->position(x, y, z);	mars->colour(0, 1, 0);

	mars->position(x, -y, z);	mars->colour(0, 0, 1);
	mars->position(x, -y, -z); mars->colour(0, 1, 0);
	mars->position(x, y, -z); mars->colour(1, 0, 0);
	////-------------------------------------
	//Back
	mars->position(x, y, -z); mars->colour(0, 0, 1);
	mars->position(x, -y, -z); mars->colour(1, 0, 0);
	mars->position(x - Vsize, y, -z);	mars->colour(0, 1, 0);

	mars->position(x, -y, -z);	mars->colour(0, 0, 1);
	mars->position(x - Vsize, -y, -z); mars->colour(0, 1, 0);
	mars->position(x - Vsize, y, -z); mars->colour(1, 0, 0);
	////-------------------------------------
	//TOP
	mars->position(x - Vsize, y, -z);	mars->colour(0, 0, 1);
	mars->position(x - Vsize, y, z); mars->colour(1, 0, 0);
	mars->position(x, y, z);	mars->colour(0, 1, 0);

	mars->position(x - Vsize, y, -z);	mars->colour(0, 0, 1);
	mars->position(x, y, z); mars->colour(0, 1, 0);
	mars->position(x, y, -z); mars->colour(1, 0, 0);
	////-------------------------------------
	//BOTTOM
	mars->position(x, -y, z);	mars->colour(0, 0, 1);
	mars->position(x - Vsize, -y, z); mars->colour(1, 0, 0);
	mars->position(x - Vsize, -y, -z); mars->colour(0, 1, 0);

	mars->position(x, -y, z); mars->colour(0, 0, 1);
	mars->position(x - Vsize, -y, -z); mars->colour(0, 1, 0);
	mars->position(x, -y, -z); mars->colour(1, 0, 0);




	//End drawing
	object->end();
	mercury->end();
	venus->end();
	earth->end();
	moon->end();
	mars->end();
	//Add manual object to the scene

	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
	//Scene node - manipulate object to move or rotate

	nodeSun = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeSun->attachObject(object);

	nodeMercury = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMercury->attachObject(mercury);

	nodeVenus = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeVenus->attachObject(venus);

	nodeEarth = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeEarth->attachObject(earth);

	nodeMoon = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMoon->attachObject(moon);

	nodeMars = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	nodeMars->attachObject(mars);



	mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5f));


	Light *light = mSceneMgr->createLight();
	light->setType(Light::LightTypes::LT_POINT);
	light->setDiffuseColour(ColourValue(1.0f, 0.7f, 0));
	light->setAttenuation(325, 0.0f, 0.014, 0.0007);
	light->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	light->setPosition(Vector3(0, 0, 0));

	
}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	/*
	Vector3 movement = Vector3::ZERO;
	int mAcceleration = 10;
	//Straightforward movement
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		movement.x += 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J)) {

		movement.x -= 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_I)) {

		movement.y += 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_K)) {

		movement.y -= 10;
	}

	//Diagonal Movement
	if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;

	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}

	movement *= evt.timeSinceLastFrame;
	node->translate(movement);




	//Exercise 4-1
	//Rotate Y
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8) && mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(0, 1, 0), Radian(rotation));

	}
	//Rotate X
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4) && mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(1, 0, 0), Radian(rotation));

	}
	//Exercise 4-2
	//Rotate away from Y
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{
		int x = 10 * cos(30) + -10 * sin(30);
		int z = 10 * -sin(30) + -10 * cos(30);
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(x, 0, z), Radian(rotation));
	}
	//Scenenode *cube = mSceneMgr->getRootSceneNode()->createChildSceneNode();


	return true;

	*/


	//Rotation						//Revolution
	//Mercury = 59 days				88 days			20 sec
	//Venus = 243 days				225 days		50 sec
	//Earth = 23hr					365 days		1 mintues
	//Mars = 24hr					687 days		2 minutes
	//Moon = 27 days				27 days			5 SEC
	//Sun = 28 days							------


	Degree rotation = Degree(25 * evt.timeSinceLastFrame);
	nodeSun->rotate(Vector3(0, 1, 0), Radian(rotation));


	rotation = Degree(18 * evt.timeSinceLastFrame);
	nodeMercury->rotate(Vector3(0, 1, 0), Radian(rotation));

	rotation = Degree(7 * evt.timeSinceLastFrame);
	nodeVenus->rotate(Vector3(0, 1, 0), Radian(rotation));


	rotation = Degree(5 * evt.timeSinceLastFrame);
	nodeEarth->rotate(Vector3(0, 1, 0), Radian(rotation));

	rotation = Degree(3 * evt.timeSinceLastFrame);
	nodeMoon->rotate(Vector3(0, 1, 0), Radian(rotation));

	rotation = Degree(2 * evt.timeSinceLastFrame);
	nodeMars->rotate(Vector3(0, 1, 0), Radian(rotation));



	/*int x = 20 * cos(45) + 1 * sin(45);
	int z = 5 * -sin(45) + 1 * cos(45);*/

	//Degree rotation = Degree(45 * evt.timeSinceLastFrame);
	//node->rotate(Vector3(x, 0, z), Radian(rotation));


	/*int x = 50 * cos(45 * evt.timeSinceLastFrame) + 20 * sin(45 * evt.timeSinceLastFrame);
	int z = 50 * -sin(45 * evt.timeSinceLastFrame) + 20 * cos(45 * evt.timeSinceLastFrame);*/

	return true;
}
ManualObject * TutorialApplication::createProceduralObject(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject();

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	myManualObjectMaterial->setReceiveShadows(false);
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1, 1, 1, 0);

	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	// Create a circle by using rotation formula to find the points
	// And 0,0 as center

	// Store the vertices in a vector first (needed later)
	std::vector<Vector3> vertices;

	// Add the first vertex at 0,0
	vertices.push_back(Vector3::ZERO);

	// Compute for the vertices first
	int NUM_POINTS = 8;
	float anglePerPoint = 360 / NUM_POINTS;
	float radius = size / 2;
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		float currentAngle = anglePerPoint * i;
		// Formula: NewX = OldX x cos(theta) - OldY x sin (theta)
		float x = radius * Math::Cos(Radian(Degree(currentAngle))) - radius * Math::Sin(Radian(Degree(currentAngle)));
		// Formula: NewY = OldX x sin(theta) + OldY x cos (theta)
		float y = radius * Math::Sin(Radian(Degree(currentAngle))) + radius * Math::Cos(Radian(Degree(currentAngle)));
		Vector3 vert(x, y, 0);
		vertices.push_back(vert);
	}

	// Plot vertices
	for (int i = 0; i < NUM_POINTS + 1; i++)
	{
		manual->position(vertices[i]);
		manual->colour(ColourValue::White);
		// Compute for normals
		if (vertices[i] == Vector3::ZERO)
			manual->normal(Vector3(0, 0, 1));
		else
			Vector3 normal = generatePolygonNormal(vertices[0], vertices[i], vertices[(i + 1) % NUM_POINTS]);
	}

	// Plot indices
	for (int i = 1; i <= NUM_POINTS + 1; i++)
	{
		// For every two points, we need to make one triangle (along with center 0,0,0 at index 0)
		manual->index(0);
		manual->index(i);
		manual->index(i + 1);
	}
	// Loop back for last triangle
	manual->index(0);
	manual->index(NUM_POINTS);
	manual->index(1);

	manual->end();
	return manual;
}
Vector3 TutorialApplication::generatePolygonNormal(Vector3 v0, Vector3 v1, Vector3 v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;

	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
