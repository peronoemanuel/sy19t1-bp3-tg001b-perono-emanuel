/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
//using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)


	//Create a manual object
	ManualObject *object = mSceneMgr->createManualObject();
	//Begin drawing
	object->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);

	int size = 10;
	//Draw your object here
	//object->position(-(size / 2), -(size / 2), size / 2);
	//object->colour(ColourValue::White);
	//object->normal(Vector3(0, 0, 1));
	//object->position((size / 2), -(size / 2), size / 2);
	//object->normal(Vector3(0, 0, 1));
	//object->position((size / 2), -(size / 2), size / 2);
	//object->normal(Vector3(0, 0, 1));


	//-------------------------------------
	//FRONT
	object->position(10, -10, 10);	//right
	object->colour(ColourValue::White);
	object->normal(Vector3(0, 0, 1));
	object->position(-10, 10, 10);	//top
	object->normal(Vector3(0, 0, 1));
	object->position(-10, -10, 10); //left  
	object->normal(Vector3(0, 0, 1));

	object->position(10, -10, 10);	//bottom
	object->normal(Vector3(0, 0, 1));
	object->position(10, 10, 10);	//right
	object->normal(Vector3(0, 0, 1));
	object->position(-10, 10, 10); //left  
	object->normal(Vector3(0, 0, 1));
	//-------------------------------------
	//LEFT
	object->position(-10, 10, -10);	//left left top
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, -10, 10);	//left right bottom
	object->colour(0, 1, 0); // Green

	object->position(-10, 10, -10);	//left left top
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, 10);	//left right bottom
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, 10); //left  right top
	object->colour(1, 0, 0); // Red

	//-------------------------------------
	//RIGHT
	object->position(10, -10, 10);	//right left bottom
	object->colour(0, 0, 1); // Blue
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	object->position(10, 10, 10);	//right left top
	object->colour(0, 1, 0); // Green


	object->position(10, -10, 10);	//right left bottom
	object->colour(0, 0, 1); // Blue
	object->position(10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//Back
	object->position(10, 10, -10);	//right left top 
	object->colour(0, 0, 1); // Blue
	object->position(10, -10, -10); //right  right bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, 10, -10);	//left right top
	object->colour(0, 1, 0); // Green

	object->position(10, -10, -10);	//right right bottom
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10);	//left right bottom
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, -10); //left  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//TOP
	object->position(-10, 10, -10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(-10, 10, 10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(10, 10, 10);	//right right bottom
	object->colour(0, 1, 0); // Green

	object->position(-10, 10, -10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(10, 10, 10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//BOTTOM
	object->position(10, -10, 10);	// right left bottom 
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, 10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green

	object->position(10, -10, 10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, -10, -10); //right  right top
	object->colour(1, 0, 0); // Red


	//Another tirangle -> Square
	//object->position(10, 10, 0); //top
	//object->position(0, 10, 0); //left 
	//object->position(10, 0, 0);	//right

	//object->position(0, 0, 0); //left  
	//object->position(10, 0, 0);	//right
	//object->position(0, 10, 0);	//top


	//You can also use RGB
	//TREAT 1 as 255

	//End drawing
	object->end();
	//Add manual object to the scene

	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
	//Scene node - manipulate object to move or rotate
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(object);

	mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5f));
	Light *light = mSceneMgr->createLight();
	light->setType(Light::LightTypes::LT_POINT);
	light->setDiffuseColour(ColourValue(1.0f, 0.7f, 0));
	light->setAttenuation(325, 0.0f, 0.014, 0.0007);
	light->setPosition(Vector3(0, 0, 0));




	//Move
	//node->translate(20, 0, 0);




}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	Vector3 movement = Vector3::ZERO;


	int mAcceleration = 10;
	//Straightforward movement
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		movement.x += 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J)) {

		movement.x -= 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_I)) {

		movement.y += 10;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_K)) {

		movement.y -= 10;
	}
	
	//Diagonal Movement
	if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
		
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}

	movement *= evt.timeSinceLastFrame;
	node->translate(movement);


	

	//Exercise 4-1
	//Rotate Y
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8) && mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
	{

		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(0, 1, 0), Radian(rotation));

	}
	//Rotate X
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4) && mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
	{
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(1, 0, 0), Radian(rotation));

	}
	//Exercise 4-2
	//Rotate away from Y
	else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
	{
		int x = 10 * cos(30) + -10* sin(30);
		int z = 10 * -sin(30) + -10 * cos(30);
		Degree rotation = Degree(30 * evt.timeSinceLastFrame);
		node->rotate(Vector3(x, 0, z), Radian(rotation));
	}
	//Scenenode *cube = mSceneMgr->getRootSceneNode()->createChildSceneNode();

	
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
