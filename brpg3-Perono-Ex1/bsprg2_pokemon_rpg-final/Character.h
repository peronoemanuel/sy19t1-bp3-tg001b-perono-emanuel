#pragma once
#include "Weapon.h"
#include <string>
using namespace std;

class Character {

public:
	//constructor
	Character();// -  initializes objects of a class.
	~Character();
	Character(string _name, int _hp, int _mp, int _pow, 
		Weapon *weapon);

	void displayStats();

	//Combat
	void attack(Character *enemy);
	//bool isDead(Character *enemy);


private:
	string name;
	int level;
	int hp;
	int mp;
	int pow;
	int basedamage;
	string weapon;
};


