#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Character.h"
#include "Weapon.h"
#include <windows.h>
using namespace std;

int getRandGen(int i, int j) {
	return rand() % i + j;
}

//string getName() {
//	string name;
//	cout << "Please enter your name?\n";
//	cout << ">";
//	getline(cin, name);
//	cin.ignore();
//	system("cls");
//}

//MAIN PROGRAM-------------------------------------------------------------------------------
int main() {
	//Initializaion---------------------------------------------

	//struct
	Character *player = new Character("Palico", 100, 150, 10
	, new Weapon("Sword", 2, 1));

	Character *monster = new Character("Ruiner Nergigante", 130, 90, 15
	, new Weapon("Fang", 2, 1));

	//gandalf->displayStats();

	//Class
	/*Wizard gandalf("Gandalf",10 ,450, 50);
	Wizard saruman("Saruman", 9, 350, 75);
	gandalf.displayStats();
	saruman.displayStats();*/
	int temp = 0;

	player->displayStats();
	monster->displayStats();
	cout << "Fight will now commence!\n\n";
	cin.ignore();
	system("cls");
	cout << "==========================\n";
	while (temp != 5) {
		monster->attack(player);
		player->attack(monster);
		cout << endl;
		player->displayStats();
		monster->displayStats();
		system("pause");
		system("cls");
		temp++;
	}

	delete player;
	delete monster;

	system("pause");
	return 0;
}
