#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Weapon.h"
#include "Character.h"
using namespace std;

Weapon::Weapon() {
	name = "";
	weaponDamage = 0;
	goldCost = 0;
}
Weapon::Weapon(string _name, int _damage, int _gold) {

	name = _name;
	weaponDamage = _damage;
	goldCost = _gold;
}
Weapon::~Weapon(){
	//delete weapon;

}

