#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Weapon.h"
#include "Character.h"
using namespace std;

Character::Character() {
	name = "";
	level = 0;
	hp = 0;
	pow = 0;
	currentWeapon = 0;
}
Character::Character(string _name, int _hp, int _mp, int _pow, Weapon *weapon) {
	name = _name;
	hp = _hp;
	mp = _mp;
	pow = _pow;
	currentWeapon = weapon;
}
void Character::displayStats() {
	cout << "Player \"" << name << "\" stats: " << endl;
	cout << "----------------------\n";
	cout << "HP: " << hp << endl;
	cout << "MP: " << mp << endl;
	cout << "Damage: " << pow << endl << endl;
}

void Character::attack(Character *enemy) {
	cout << "\"" << name << "\" attack \n";
	enemy->hp -= pow + _Weapon->getDamage(); ;
	cout << " \"" << enemy->name << "\" received " << pow << " damage!\n";
	cout << " \"" << enemy->name << "\" has only " << enemy->hp << " hp left.\n";
}
Character::~Character(){
	//delete character
}
//bool Character::isDead(){

//	return hp = 0;
//}
