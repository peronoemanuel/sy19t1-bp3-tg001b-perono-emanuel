#ifndef GDSTRUCT_POKEMON_H
#define GDSTRUCT_POKEMON_H

#include <string>
using namespace std;

class Pokemon {

public:
	vector<string>pokemons;
	string name;
	int hp;
	int baseHp;
	int level;
	int baseDamage;
	float exp;
	float expToNextLevel;
	float expToGive;
	string element;

	int money;
	//Item ShopS
	int potion; // heal summoner
	int reviveOrb; // revive dead
	int demonPowder; // boost attack
	int pokeBalls;
	
	void itemShop();
	void useItem();  
	void inventory();

	//Elemental Weaknesses
	int grass, fire, water;
	int Bug, flying, normal, electric, fairy, ground, pyshic, fighting;

	float temp;

	void elementMultiplier(Pokemon *enemy);

	//constructor
	Pokemon();// -  initializes objects of a class.
	Pokemon(int choice);//Choice

	void pokemonAssignStats(int choice);
	void displayStats();

	//Combat
	void attack(Pokemon *enemy);
	bool isDead(Pokemon *enemy);

	//Level Up
	void levelUp();
	void evolution();
};

#endif

