#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Pokemon.h"
using namespace std;

Pokemon::Pokemon() {
	name = "";
	element = "";
	level = rand() % 10 + 1;
	baseHp = 20;
	baseDamage = 11;
	exp = 0;
	expToGive = 10;
	expToNextLevel = 30;
	pokemonAssignStats(4);

	money = 0;
	potion = 0;
	demonPowder = 0;
	reviveOrb = 0;
	pokeBalls = 0;

	//Level Adjustment Based
	for (int i = 0; i < level; i++) {
		baseHp *= 1.15;
		baseDamage *= 1.1;
		expToNextLevel *= 1.2;
		expToGive *= 1.2;
	}
	hp = baseHp;

	grass = 0, fire = 0, water = 0;
	Bug = 0, flying = 0, normal = 0, electric = 0, fairy = 0, ground = 0, pyshic = 0, fighting = 0;

}
Pokemon::Pokemon(int choice) {
	name = "";
	element = "";
	hp = 0;
	baseHp = 0;
	level = 0;
	baseDamage = 0;
	exp = 0;
	expToNextLevel = 0;
	expToGive = 0;
	pokemonAssignStats(choice);

}
void Pokemon::pokemonAssignStats(int choice) {
	string names[16] = { "Caterpie", "Weedle", "Pidgey", "Rattata", "Spearow", "Pikachu", "Nidoran", 
		"Jigglypuff", "Diglett", "Meowth", "Psyduck", "Abra", "Machop", "Voltorb", "Cubone", "Magikarp" };
	string elements[16] = { "Bug", "Bug", "Flying", "Normal", "Flying", "Electric", "Normal",
		"Fairy", "Ground", "Normal", "Psychic", "Psychic", "Fighting", "Electric", "Ground", "Water" };
	//POKEMON STATS----------------------------
	//Bulbasaur
	if (choice == 1) {
		name = "Bulbasaur";
		element = "Grass";
		hp = 51;
		baseHp = 51;
		level = 5;
		baseDamage = 11;
		exp = 0;
		expToNextLevel = 60;
		expToGive = 10;
	}
	//Charmander
	else if (choice == 2) {
		name = "Charmander";
		element = "Fire";
		hp = 50;
		baseHp = 50;
		level = 5;
		baseDamage = 12;
		exp = 0;
		expToNextLevel = 60;
		expToGive = 10;
	}
	//Squirtle
	else if (choice == 3) {
		name = "Squirtle";
		element = "Water";
		hp = 50;
		baseHp = 50;
		level = 5;
		baseDamage = 11;
		exp = 0;
		expToNextLevel = 60;
		expToGive = 10;
	}
	//Randomized Pokemon
	else if (choice == 4) {
		int random = rand() % 15 + 0;
		name = names[random];
		element = elements[random];
		expToGive = 10;
	}
	money = 1000;
	potion = 3;
	demonPowder = 1;
	reviveOrb = 1;
	pokeBalls = 5;
}
void Pokemon::displayStats() {
	cout << "-----------------------------------------\n";
	cout << "----\""<< name << "\"stats----\n";
	cout << "Type:  " << element << endl;
	cout << "Lvl:   " << level << endl;
	cout << "Hp:    " << hp << "/" << baseHp;
	if (hp < 0) {
		cout << "  (FAINTED)" << endl;
	}
	else {
		cout << "  (ALIVE)" << endl;
	}
	cout << "Attck: ~" << baseDamage << endl;
	cout << "Exp:   " << exp << "/"<< expToNextLevel << endl;
	cout << "-----------------------------------------\n\n";
}
//COMBAT-----
void Pokemon::elementMultiplier(Pokemon *enemy) {
	temp = 1;
	//FIRE
	if (this->element == "Fire") {
		if ((enemy->element == "Water")|| (enemy->element == "Fire")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Bug") || (enemy->element == "Grass")) {
			temp = 2;
		}
	}
	//WATER
	else if (this->element == "Water") {
		if ((enemy->element == "Water") || (enemy->element == "Grass")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Ground")|| (enemy->element == "Fire")) {
			temp = 2;
		}
	}
	//GRASS
	else if (this->element == "Grass") {
		if ((enemy->element == "Flying") || (enemy->element == "Fire")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Grass") || (enemy->element == "Bug")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Ground")|| (enemy->element == "Water")) {
			temp = 2;
		}
	}
	//BUG
	else if (this->element == "Bug") {
		if ((enemy->element == "Flying") || (enemy->element == "Fighting")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Grass")|| (enemy->element == "Fairy")) {
			temp = 0.5;
		}
		else if (enemy->element == "Psychic") {
			temp = 2;
		}
	}
	//ELECTRIC
	else if (this->element == "Electric") {

		if ((enemy->element == "Ground") || (enemy->element == "Grass")) {
			temp = 0.5;
		}
		else if (enemy->element == "Electric") {
			temp = 0.5;
		}
		else if ((enemy->element == "Water") || (enemy->element == "Flying")) {
			temp = 2;
		}
	}
	//FAIRY
	else if (this->element == "Fairy") {
		if (enemy->element == "Fire") {
			temp = 0.5;
		}
		else if (enemy->element == "Fighting") {
			temp = 2;
		}
	}
	//GROUND
	else if (this->element == "Ground") {
		if ((enemy->element == "Bug") || (enemy->element == "Grass")) {
			temp = 0.5;
		}
		else if ((enemy->element == "Fire") || (enemy->element == "Electric")) {
			temp = 2;
		}
	}
	//PSYCHIC
	else if (this->element == "Psychic") {
		if (enemy->element == "Psychic") {
			temp = 0.5;
		}
		else if (enemy->element == "Fighting") {
			temp = 2;
		}
	}
	//FIGHTING
	else if (this->element == "Fighting") {
		if (enemy->element == "Psychic") {
			temp = 0.5;
		}
		else if ((enemy->element == "Flying") || (enemy->element == "Bug")) {
			temp = 0.5;
		}
		else if (enemy->element == "Normal") {
			temp = 2;
		}
}
	
	//TEMP
	if (temp == 2) {
		cout << this->name << "'s attack will deal double damage!\n";
	}
	else if (temp == 0.5){
		cout << "\n\ntemp test: "<< temp << endl;
		cout << enemy->name << " can withstand " << this->element << " attacks!\n";
	}
}
void Pokemon::attack(Pokemon *enemy) {
	int chanceToMiss = rand() % 100 + 1;
	cout << "----------------------------------------------------\n";
	if (chanceToMiss > 20) {
		cout << this->name << " attacked " << enemy->name << "!\n";
		//Elemental Multiplier
		elementMultiplier(enemy);
		cout << enemy->name << " took " << this->baseDamage * temp << " damage!\n";
		enemy->hp -= (this->baseDamage * temp);
		cout << enemy->name << " has " << enemy->hp << " / " << enemy->baseHp << " left.\n";
	}
	else {
		cout << this->name << " attack miss!\n\n";
	}
	cout << "----------------------------------------------------\n\n";
	system("pause");
}
bool Pokemon::isDead(Pokemon *enemy){
	if (enemy->hp < 1) {
		cout << enemy->name << " fainted...\n";
		cout << this->name <<" wins!\n";
		for (int i = 0; i < this->level; i++) {
			this->expToGive *= 1.2;
		}
		cout << this->name << " gains "<< this->expToGive << " Exp. Points!\n\n";
		this->exp += this->expToGive;
		return true;
	}
	else {
		return false;
	}
}
//LevelUp----Evolution
void Pokemon::levelUp() {
	int tempLevel;
	do {
		if (exp >= expToNextLevel) {
			exp -= expToNextLevel;
			cout << name << " Level Up! \n";
			tempLevel = level;
			level += 1;
			cout << name << " is now level " << level << endl;
			if (expToNextLevel < exp) {
				exp -= expToNextLevel;
			}
			for (int i = tempLevel; i < level; i++) {
				baseHp *= 1.15;
				baseDamage *= 1.1;
				expToNextLevel *= 1.2;
				expToGive *= 1.2;
			}
		}
	} while (exp > expToNextLevel);
}
void Pokemon::evolution() {
	//evolution
	string namesEvolved[6] = { "Charmaleon", "Charmander","Ivysaur","Venusaur","Wartortle","Blastoise" };
	cout << "\nExp to next level: " << this->expToNextLevel << endl;
	if ((this->name == "Charmander" ) && (this->level == 7 || this->level == 10)) {
		cout << "WHAT! " << name << " is evolving!!!\n\n";
		cout << name << " evolved into... ";
		if (this->level = 7) {
			//Charmaleon
			name = namesEvolved[0];
		}
		else if (this->level = 10) {
			//Charmander 
			name = namesEvolved[1];
		}
	}
	else if (this->name == "Bulbasaur") {
		cout << "WHAT! " << name << " is evolving!!!\n\n";
		cout << name << " evolved into... ";
		if (this->level = 7) {
			//Ivysaur
			name = namesEvolved[2];
		}
		else if (this->level = 10) {
			//Venusaur 
			name = namesEvolved[3];
		}
	}
	else if (this->name == "Squirtle") {
		cout << "WHAT! " << name << " is evolving!!!\n\n";
		cout << name << " evolved into... ";
		if (this->level = 7) {
			//Wartortle
			name = namesEvolved[4];
		}
		else if (this->level = 10) {
			//Blastoise 
			name = namesEvolved[5];
		}
	}
	cout << name << endl << endl;
}
//Item Shop - Inventory
void Pokemon::itemShop() {
	char buyItem;
	do {
		cout << "Welcome to the PokeMart!\n"
			<< "You currently have : " << money << " gold\n"
			<< "What would you like ?\n\n"
			<< "1.Potion	- 80 gold\n"
			<< "2.Demon Powder  - 100 gold\n"
			<< "3.Pokeball	- 70 gold\n"
			<< "4.Revive Orb	- 500 gold\n"
			<< "5.Exit Shop	 - (t) \n\n";
		inventory();
		cout << ">";
		cin >> buyItem;
		switch (buyItem) {
		case 'q':
			if (money >= 80) {
				money -= 80;
				potion += 1;
			}
			else {
				cout << "You don't have enough money to buy Potion\n";
			}
			break;
		case 'w':
			if (money >= 100) {
				money -= 100;
				demonPowder += 1;
			}
			else {
				cout << "You don't have enough money to buy Demon Powder\n";
			}
			break;
		case 'e':
			if (money >= 70) {
				money -= 70;
				pokeBalls += 1;
			}
			else {
				cout << "You don't have enough money to buy Pokeballs\n";
			}
			break;
		case 'r':
			if (money >= 500) {
				money -= 500;
				reviveOrb += 1;
			}
			else {
				cout << "You don't have enough money to buy Revive Orb\n";
			}
			break;
		case 't':
			cout << "\nWe hope to see you again!\n\n";
			break;
		}
		system("pause");
		system("cls");
	} while (buyItem != 't');
	
}
void Pokemon::inventory() {
	cout << "---INVENTORY----\n";
	cout << "["<<potion << "] Potion	 - (q)\n";
	cout << "["<<demonPowder  <<"] Demon Powder - (w)\n";
	cout << "[" << pokeBalls << "] Pokeballs    - (e)\n";
	cout << "[" << reviveOrb << "] Revive Orb   - (r)\n\n";
}
void Pokemon::useItem() {
	char selectItem;
	cout << ">";
	cin >> selectItem;
	switch (selectItem) {
	case 'q':
		if (potion > 0) {
			potion -= 1;
			cout << "You use Potion on " << name << endl << name << " heals 40 hp!\n";
			hp += 40;
		}
		else {
			cout << "\nYou have 0 potion!\n";
		}
		break;
	case 'w':
		if (potion > 0) {
			demonPowder -= 1;
			baseDamage += 10;
			cout << "You use Demon Powde on " << name << endl << name << " deals additional 10 damage boost!\n";
		}
		else {
			cout << "\nYou have 0 Demon Powder!\n";
		}
		break;
	case 'e':
		cout << "\n*Use Pokeball to catch wild pokemons!\n";
		break;
	case 'r':
		if (potion > 0) {
			reviveOrb -= 1;
			hp = baseHp;
		}
		else {
			cout << "\nYou have 0 Revive Orb!\n";
		}
		break;
	}
	system("pause");
	system("cls");
	
}