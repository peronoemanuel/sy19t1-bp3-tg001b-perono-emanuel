#ifndef BASPRG3_WEAPON_H
#define BASPRG3_WEAPON_H

#include <string>
using namespace std;

class Character;
class Weapon {

public:

	//weapon
	string nameWeapon;
	int damage;
	int minDamage;
	int maxDamage;


	void attack(Weapon *enemy);
};

#endif