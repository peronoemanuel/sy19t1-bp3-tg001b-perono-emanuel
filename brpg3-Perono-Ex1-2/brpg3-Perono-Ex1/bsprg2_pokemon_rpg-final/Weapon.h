#pragma once

#include <string>
using namespace std;

class Weapon {

public:
	//constructor
	Weapon();// -  initializes objects of a class.
	~Weapon();
	Weapon(string _name, int _damage, int _gold);//Choice

	int getDamage();
	string getName();

private:
	string weaponName;
	int weaponDamage;
	int goldCost;

};


