#pragma once

#include <string>
#include <iostream>
using namespace std;

class Weapon;
class Character;
class Skill {

public:
	//constructor
	Skill();// -  initializes objects of a class.
	~Skill();
	Skill(string _name, int _mpCost, int _damage);//Choice


	// Copy this function to all child classes
	virtual void cast(Character *caster, Character *target);
	// virtual means a function can be OVERWRITTEN by child classes

	//int getSkillDamage();
	//string getSkillName();

// protected means these variables are PRIVATE outside but can be inherited
//protected:
//	string _name;
//
//protected:
//
//	int _mpCost;
//	string _name;

	int getDamage();

private:
	string skillName;
	int mpCost;
	int skillDamage;
};