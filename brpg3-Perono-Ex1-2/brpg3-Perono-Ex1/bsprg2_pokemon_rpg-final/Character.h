#pragma once
#include "Weapon.h"
#include <string>
using namespace std;

class Skill;
class Character {

public:
	//constructor
	Character();// -  initializes objects of a class.
	~Character();
	Character(string _name, int _hp, int _mp, int _pow, 
		Weapon *weapon);

	void displayStats();

	//Combat
	void attack(Character *enemy);
	bool isDead(Character *enemy);

	void equipWeapon(Weapon *weapon);

	int skillChance;
	int skillDamage;
	int lifeStealSkill;

	// Accessor method
	void addExp(int value);
	void setName(string name);
	int getExp();

private:
	// Add a vector of skills here
	vector<Skill*> skills;

	string name;
	int level;
	int hp;
	int baseHp;
	int mp;
	int pow;
	int basedamage;
	Weapon *currentWeapon;

	Skill *skill;

};


