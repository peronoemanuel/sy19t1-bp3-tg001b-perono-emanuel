#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Weapon.h"
#include "Character.h"
#include "Normal.h"
#include "Critical.h"
#include "Skill.h"


Skill::Skill(){

	skillName = "";
	mpCost = 0;
	skillDamage = 0;
}

Skill::Skill(string _name, int _mpCost, int _damage) {
	skillName = _name;
	mpCost = _mpCost;
	skillDamage = _damage;
}

Skill::~Skill(){
	//delete Skill;
}

void Skill::cast(Character *caster, Character *target){
	cout << "Skill used! " << endl;

	//Normal Attack call
	//Critical Attack call
}