#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
//
#include "Weapon.h"
#include "Character.h"
#include "Skill.h"
#include "Normal.h"
#include "Critical.h"
using namespace std;

Character::Character() {
	name = "";
	level = 0;
	hp = 0;
	baseHp = 0;
	pow = 0;
	currentWeapon = NULL;
	//skillDamage = 0;
	skillChance = 0;
	lifeStealSkill = 0;
	skill = NULL;
}
Character::Character(string _name, int _hp, int _mp, int _pow, Weapon *weapon) {
	name = _name;
	baseHp = hp;
	hp = _hp;
	mp = _mp;
	pow = _pow;
	currentWeapon = weapon;
	srand(time(NULL));
}
void Character::displayStats() {
	cout << "Player \"" << name << "\" stats: " << endl;
	cout << "----------------------\n";
	cout << "HP: " << hp;
	if (hp < 1) {
		cout << " (fainted)";
	}
	cout << "\nMP: " << mp << endl;
	cout << "ATck: " << pow << endl;
	cout << "Weapon Damage: " << currentWeapon->getDamage() << endl << endl;
}

Character::~Character() {
	delete currentWeapon;
}

void Character::attack(Character *enemy) {
	int attack;
	int vengeance;
	cout << "==========================\n";
	cout << "\"" << name << "\" attacks with "<<"["<< currentWeapon->getName() <<"]\n";
	cin.ignore();
	//skillChance = rand() % 100 + 1;
	////Critical Attack
	//if (skillChance < 21) {
	//	cout << "[" << currentWeapon->getName() << "] Crits! \n";
	//	cout << "~ Weapon will deal 200% damage!\n";
	//	skillDamage = 2;
	//}
	////Syphon
	//else if (skillChance > 20 && skillChance < 41){
	//	skillDamage = 1;
	//	cout << "~ Weapon Skill activated: Syphon\n";
	//	cout << "Attack will deal 25% weapon damage lifesteal!!\n";
	//	lifeStealSkill = (pow + currentWeapon->getDamage() * skillDamage) * 0.25;
	//	cout << this->name << " absorbs " << lifeStealSkill << " HP!" << endl;
	//	cout << this->name << " has now " << lifeStealSkill + this->hp << " Hp\n";
	//	this->hp += lifeStealSkill;
	//}
	////Vengeance
	//else if (skillChance > 42 && skillChance < 69) {
	//	vengeance = this->baseHp * .25;
	//	this->hp -=vengeance;
	//	if (this->hp <1) {
	//		this->hp = 1;
	//	}
	//	cout << "~ Weapon Skill activated: Vengeance\n";
	//	cout << "Use 25% baseHp to add 200% weapon damage!!\n";
	//	cout << " \""<<this->name << "\"" << " hp deducted...\n";
	//	cout << "Attack deals 300% weapon damage!\n";
	//	cout << " \"" << this->name<< "\"" << " has now " << this->hp << " Hp\n";
	//	skillDamage = 3;
	//}
	////Normal Attack
	//else {
	//	cout << "~ Normall Attack!!!\n";
	//	skillDamage = 1;
	//}

	//Attack
	attack = pow + (currentWeapon->getDamage() * skill->skillDamage());
	enemy->hp -= attack;


	cout << " \"" << enemy->name << "\" received " << attack << " damage!\n";
	cout << " \"" << enemy->name << "\" has only " << enemy->hp << " hp left.\n\n";
}

void Character::equipWeapon(Weapon * weapon){
	// Check if we have an old weapon
	if (currentWeapon != NULL) {
		delete currentWeapon;
	}
	// Equip new weapon
	currentWeapon = weapon;
}

bool Character::isDead(Character *enemy){
	if (enemy->hp < 1) {
		cout << enemy->name << " fainted...\n";
		cout << this->name << " wins!\n";
		return true;
	}
	else {
		return false;
	}
}
