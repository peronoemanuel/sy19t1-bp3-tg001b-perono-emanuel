#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include <windows.h>
//
#include "Character.h"
#include "Weapon.h"
#include "Skill.h"
#include "Normal.h"
#include "Critical.h"
using namespace std;

//MAIN PROGRAM-------------------------------------------------------------------------------
int main() {
	//int randSkills;
	//Initializaion---------------------------------------------
	Character *player = new Character("Palico", 100, 150, 10
	, new Weapon("Sword", 20, 1));

	Character *monster = new Character("Ruiner Nergigante", 130, 90, 15
	, new Weapon("Fang", 10, 1));

	bool isDead = false;
	bool playerFainted = false;

	player->displayStats();
	monster->displayStats();
	cout << "Fight will now commence!\n\n";
	cin.ignore();
	system("cls");

	//Normal *skill1 = new Normal();
	//Critical *skill2 = new Critical();
	//vector<Skill*> skills;
	//skills.push_back(skill1);	//Normal Attack
	//skills.push_back(skill2);	//Critical Attack

	//Game Loop until either are dead
	while (isDead != true) {
		//randSkills = getRandGen(2, 0);
		//cout << "\nRand Skills: " << randSkills << endl;
		//skills[randSkills]->cast(player, monster); //Cast skill randomly

		monster->attack(player);
		if (monster->isDead(player) == true) {
			isDead = true;
			playerFainted = true;
		}
		else {
			//randSkills = getRandGen(2, 0);
			//cout << "\nRand Skills: " << randSkills << endl;
			//skills[randSkills]->cast(player, monster); //Cast skill randomly

			player->attack(monster);
		}
		if (player->isDead(monster) == true) {
			isDead = true;
			playerFainted = false;
		}
		cout << endl;
		cin.ignore();
		system("cls");

		player->displayStats();
		monster->displayStats();
		cin.ignore();
		system("cls");

	}
	if (playerFainted == true) {
		delete player;
	}
	else
		delete monster;
		
	cout << "\nThank you!\n\n";
	system("pause");
	return 0;
}
