#pragma once
#include <iostream>
#include "Skill.h"

using namespace std;

class Normal : public Skill{
public:
	Normal();
	Normal(string name, int mpCost, int damageMultiplier);
	~Normal();

	// Same function from parent class
	void cast(Character *caster, Character *target);

	int getDamage();

private:
	string skillName;
	int skillDamage;
	int mpCost;
};

