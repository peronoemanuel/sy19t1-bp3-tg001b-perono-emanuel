#include <stdio.h>
#include <cmath>
#include <ctime>
#include <list> 
#include <vector>
#include "Weapon.h"
#include "Character.h"
#include "Normal.h"
#include "Skill.h"
//#include "Syphon.h"
using namespace std;

Normal::Normal() {
	skillName = "";
	skillDamage = 0;
	mpCost = 0;
}
//Use parent constructor
Normal::Normal(string name, int mpCost, int damageMultiplier) : Skill(name, mpCost, damageMultiplier) {

}


Normal::~Normal() {
	//delete Normal;

}

void Normal::cast(Character *caster, Character *target) {
	cout << "Normal Attack was used! " << endl;
	skillDamage = 1;

}
