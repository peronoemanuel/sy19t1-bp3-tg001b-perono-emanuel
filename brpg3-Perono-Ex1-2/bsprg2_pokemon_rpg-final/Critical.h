#pragma once
#include <iostream>
#include "Skill.h"

using namespace std;

class Critical : public Skill{
public:
	Critical();
	Critical(string name, int mpCost, float damageMultiplier);
	~Critical();

	// Same function from parent class
	void cast(Character *caster, Character *target);

	int getDamage();

private:
	string skillName;
	int skillDamage;
};
