#pragma once

#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>
#include "BaseApplication.h"
using namespace Ogre;


class Planet : public BaseApplication

{
public:

	Planet(SceneNode *node);
	~Planet();
	
	//static Planet *createPlanet(SceneManager &sceneManager, float size, ColourValue colour);
	
	void update(const FrameEvent & evt);

	SceneNode * getNode();
	void setParent(Planet *parent);
	Planet *getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);


protected:
	virtual void createPlanet(void);
	//bool frameStarted(const FrameEvent &evt);

private:
		SceneNode *mNode;
		Planet *mParent;
		SceneNode *nodeSun;
		SceneNode *nodeMercury;
		SceneNode *nodeVenus;
		SceneNode *nodeEarth;
		SceneNode *nodeMoon;
		SceneNode *nodeMars;
		float mLocalRotationSpeed;
		float mRevolutionSpeed;

};

