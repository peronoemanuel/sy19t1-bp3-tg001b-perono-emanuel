/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
//using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)


	//Create a manual object
	ManualObject *object = mSceneMgr->createManualObject();
	//Begin drawing
	object->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	//Draw your object here
	
	//-------------------------------------
	//FRONT
	object->position(10, -10, 10);	//right
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, 10);	//top
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, 10); //left  
	object->colour(1, 0, 0); // Red

	object->position(10, -10, 10);	//bottom
	object->colour(0, 0, 1); // Blue
	object->position(10, 10, 10);	//right
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, 10); //left  
	object->colour(1, 0, 0); // Red
	//-------------------------------------
	//LEFT
	object->position(-10, 10, -10);	//left left top
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, -10, 10);	//left right bottom
	object->colour(0, 1, 0); // Green

	object->position(-10, 10, -10);	//left left top
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, 10);	//left right bottom
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, 10); //left  right top
	object->colour(1, 0, 0); // Red

	//-------------------------------------
	//RIGHT
	object->position(10, -10, 10);	//right left bottom
	object->colour(0, 0, 1); // Blue
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	object->position(10, 10, 10);	//right left top
	object->colour(0, 1, 0); // Green


	object->position(10, -10, 10);	//right left bottom
	object->colour(0, 0, 1); // Blue
	object->position(10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//Back
	object->position(10, 10, -10);	//right left top 
	object->colour(0, 0, 1); // Blue
	object->position(10, -10, -10); //right  right bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, 10, -10);	//left right top
	object->colour(0, 1, 0); // Green

	object->position(10, -10, -10);	//right right bottom
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10);	//left right bottom
	object->colour(0, 1, 0); // Green
	object->position(-10, 10, -10); //left  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//TOP
	object->position(-10, 10, -10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(-10, 10, 10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(10, 10, 10);	//right right bottom
	object->colour(0, 1, 0); // Green

	object->position(-10, 10, -10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(10, 10, 10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, 10, -10); //right  right top
	object->colour(1, 0, 0); // Red
	////-------------------------------------
	//BOTTOM
	object->position(10, -10, 10);	// right left bottom 
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, 10); //left  left bottom
	object->colour(1, 0, 0); // Red
	object->position(-10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green

	object->position(10, -10, 10);	//left left top 
	object->colour(0, 0, 1); // Blue
	object->position(-10, -10, -10);	//right right bottom
	object->colour(0, 1, 0); // Green
	object->position(10, -10, -10); //right  right top
	object->colour(1, 0, 0); // Red


	//Another tirangle -> Square
	//object->position(10, 10, 0); //top
	//object->position(0, 10, 0); //left 
	//object->position(10, 0, 0);	//right

	//object->position(0, 0, 0); //left  
	//object->position(10, 0, 0);	//right
	//object->position(0, 10, 0);	//top


	//You can also use RGB
	//TREAT 1 as 255

	//End drawing
	object->end();
	//Add manual object to the scene

	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(object);
	//Scene node - manipulate object to move or rotate
	node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(object);

	//Move
	//node->translate(20, 0, 0);




}

bool TutorialApplication::frameStarted(const FrameEvent & evt)
{

	int mAcceleration = 10;
	//Straightforward movement
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		node->translate(mAcceleration * evt.timeSinceLastFrame, 0, 0);
		mAcceleration++;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, 0, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(0, mAcceleration * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(0, -mAcceleration * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	
	//Diagonal Movement
	if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
		
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_I)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, 10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_L) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	else if (mKeyboard->isKeyDown(OIS::KC_J) && mKeyboard->isKeyDown(OIS::KC_K)) {

		node->translate(-mAcceleration * evt.timeSinceLastFrame, -10 * evt.timeSinceLastFrame, 0);
		mAcceleration *= mAcceleration;
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
