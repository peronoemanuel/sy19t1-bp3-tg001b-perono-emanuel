#include "ShockWave.h"

ShockWave::ShockWave(string name, int mpCost) : Skill(name, mpCost) {

}


ShockWave::~ShockWave() {

}

void ShockWave::cast(Character * caster, Character * target) {
	cout << caster->getName() << " uses " << mName << "!" << endl;


	//int damage = 50;
	for (int i = 0; i < 1; i++) {
		cout << "Shock Wave dealt " << caster->getDamage(caster, target) << " damage against " << target->getName() << endl;
	}

	target->takeDamage(caster->getDamage(caster, target));
	caster->reduceMp(mMpCost);
	system("pause");
	system("cls");

}
