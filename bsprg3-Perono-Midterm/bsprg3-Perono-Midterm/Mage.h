#pragma once
#include "Classes.h"
class Mage : public Classes
{
public:
	Mage(string name, string characClass, string teamClass, int hp, int agi);
	~Mage();

	void player(Character *caster, Character *target);
};

