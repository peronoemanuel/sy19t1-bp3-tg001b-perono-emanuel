#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Character.h"
#include "Skill.h"
#include "Classes.h"
using namespace std;

int main(){
	srand(time(NULL));

	Character *player1 = new Character("Momon", "Warrior", "SORCERER KINGDOM", 80, 150, 25,20,10,15);
	Character *player2 = new Character("Narberal", "Mage", "SORCERER KINGDOM", 75, 100, 15, 15, 5, 7);
	Character *player3 = new Character("Solution", "Assassin", "SORCERER KINGDOM", 60, 120, 30, 10, 15, 30);

	Character *enemy1 = new Character("Lauransan", "Warrior", "SLANE THEOCRACY", 90, 90, 30,15,10,15);
	Character *enemy2 = new Character("Rigrit", "Mage", "SLANE THEOCRACY", 95, 130, 15, 20, 25, 5);
	Character *enemy3= new Character("Evil Eye", "Assassin", "SLANE THEOCRACY", 100, 120, 20, 20, 25, 10);

	//_class.push_back(player1);

	// Main combat loop
	while (player1->isAlive() && enemy1->isAlive()){
		// Print remaining stats
		player1->printStats();

		player1->attack(player1,enemy1);
	/*	system("pause");
		system("cls");*/

		enemy1->printStats();
		enemy1->attack(enemy1, player1);

	}
	// Win-lose conditions
	if (player1->isAlive()){
		cout << player1->getTeamClass() << " wins!\n";
		cout << player1->getName() << " wins!";
		delete enemy1;
	}
	else if (enemy1->isAlive()){
		cout << enemy1->getTeamClass() << " wins!\n";
		cout << enemy1->getName() << " wins!";
		delete player1;
	}
	else{
		cout << "Draw!" << endl;
	}
	cout << endl << endl;
	system("pause");
	return 0;
}