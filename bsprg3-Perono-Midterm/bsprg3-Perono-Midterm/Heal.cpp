#include "Heal.h"

Heal::Heal(string name, int mpCost) : Skill(name, mpCost) {

}


Heal::~Heal() {

}

void Heal::cast(Character *caster, Character *target) {
	cout << caster->getName() << " uses " << mName << " on " << caster->getName() << endl;
	int heal = caster->getHp() * .2;
	cout << caster->getName() << " was healed for " << heal << " hp " << endl;
	caster->heal(heal);
	caster->reduceMp(mMpCost);
	system("pause");
	system("cls");
}
