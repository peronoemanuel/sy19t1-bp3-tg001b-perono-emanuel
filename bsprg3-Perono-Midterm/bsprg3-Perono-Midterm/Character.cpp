#include "Character.h"
#include "Skill.h"
#include "Warrior.h"
#include "Mage.h"


#include "BaseAttack.h"
#include "ShockWave.h"
#include "Assassinate.h"
#include "Heal.h"


Character::Character(string _name, string _characClass, string _teamClass, int _hp, int _mp, int _pow, int _vit, int _agi, int _dex){
	name = _name;
	characClass = _characClass;
	teamClass = _teamClass;
	hp = _hp;
	baseHp = hp;
	mp = _mp;
	baseMp = mp;
	randomBasePow = 0;
	damage = 0;
	pow = _pow;
	vit = _vit;
	agi = _agi;
	dex = _dex;

	_skills.push_back(new BaseAttack("Axe Smash", 0)); // Basic Attack
	_skills.push_back(new ShockWave("Shockwave", 35)); // Skill Attack
	_skills.push_back(new Assassinate("Assassinate", 50)); // Skill Attack
	_skills.push_back(new Heal("Heal", 40)); // Skill Attack
}

Character::~Character(){
	/*if (!isAlive())
		delete Character;
	for (int i = 0; i < mSkills.size(); i++)
		delete mSkills[i];
	mSkills.clear();*/
}

bool Character::doesHit(Character *caster, Character *target) {

	int randHitRate = rand() % 100 + 1;
	float hitRate = (caster->getDex() / target->getAgi()) * 100;
	//cout << "Hit Rate: " << hitRate << endl;
	//cout << "Dex: " << caster->getDex() << endl;
	//cout << "Agi: " << target->getAgi() << endl;
	if (randHitRate) {
		if (randHitRate > 19 && randHitRate < 81) {
			return true;
		}
	}
	else {
		cout << "\nAttack Miss!\n";
		return false;

	}
}
bool Character::isPlayerTurn(Character *caster, Character *target) {
	/*if () {
		return true;
	}
	else
		return false;*/
	return false;
}


void Character::attack(Character *caster, Character *target){
	int randIndex;
	Skill *skillToUse = _skills[0];
	bool isPlayerTurn = true;
	bool doesHaveMp;
	do {
		doesHaveMp = true;
		if (isPlayerTurn) {
			cout << "\nChoose an action....\n";
			cout << "=================================\n\n";
			for (int i = 0; i < 4; i++) {
				skillToUse = _skills[i];
				cout << "\t[" << i + 1 << "] " << skillToUse->getName() << " (MP Cost: " << skillToUse->getMpCost() << ")" << endl;
			}
			cout << endl << ">";
			cin >> randIndex;
			cout << endl << endl;
			randIndex -= 1;
			skillToUse = _skills[randIndex];
			cin.ignore();
		}
		else {
			int randIndex = rand() % _skills.size();
			cout << "\n\n=================================\n\n";
			skillToUse = _skills[randIndex];
		}
		if (caster->doesHit(caster, target)) {
			if (canCast(skillToUse) == true) {
				skillToUse->cast(this, target);
			}
			else {
				cout << "Failed use " << skillToUse->getName() << "! No MP!" << endl;
				system("pause");
				system("cls");
				doesHaveMp = false;
			}
		}
	} while (doesHaveMp == false);
	

}

void Character::takeDamage(int value) {
	// Only take damage greater than 0
	if (value > 0){
		hp -= value;

		// Don't allow hp to become negative
		if (hp < 0)
			hp = 0;
	}
}

void Character::reduceMp(int value){
	// Only take damage greater than 0
	if (value > 0){
		mp -= value;

		// Don't allow hp to become negative
		if (mp < 0)
			mp = 0;
	}
}

void Character::heal(int value){

	if (value > 0){
		hp += value;
	}
}
string Character::getName(){
	return this->name;
}
string Character::getCharacClass() {
	return this->characClass;
}
string Character::getTeamClass() {
	return this->teamClass;
}
int Character::getHp(){
	return this->hp;
}
int Character::getMp(){
	return this->mp;
}
int Character::getPow() {
	return this->pow;
}
int Character::getVit() {
	return this->vit;
}
int Character::getAgi() {
	return this->agi;
}
int Character::getDex() {
	return this->dex;
}
int Character::getRandomBasePow(Character *caster) {
	int maxPow = (caster->getPow() * .2) - 1;
	randomBasePow = rand() % maxPow + caster->getPow();
	return this->randomBasePow;
}
int Character::getDamage(Character *caster, Character *target) {
	float bonusDamage = 1;
	if (caster->getCharacClass() == target->getCharacClass()) {
		bonusDamage = 1;
	}
	else if (caster->getCharacClass() == "Warrior" && target->getCharacClass() == "Mage") {
		bonusDamage = 0.5;
	}
	else if (caster->getCharacClass() == "Warrior" && target->getCharacClass() == "Assassin") {
		bonusDamage = 1.5;
	}
	int damage = (target->getVit() - caster->randomBasePow) * bonusDamage;
	return damage;
}
void Character::printStats(){
	cout << "Name: " << name << endl;
	cout << "Class: " << characClass << endl;
	cout << "HP: " << hp <<"/"<<baseHp<< endl;
	cout << "MP: " << mp << "/" << baseMp << endl;
	cout << "Pow: " << pow << endl;
	cout << "Vit: " << vit << endl;
	cout << "Dex: " << dex << endl;
	cout << "Agi: " << agi << endl;
}
void Character::printTeamStats() {
	Classes *characters = _class[0];
	cout << "=====================================\n";
	cout << "TEAM: SORCERER KINGDOM\n";
	cout << "=====================================\n";
	for (int i = 0; i < 1; i++) {
		cout << characters->getName() << " [HP: "<< characters->getHp()<<"]"<<endl;
	}
	cout << "" << endl;
	cout << "=====================================\n";
	cout << "TEAM: SLANE THEOCRACY\n";
	cout << "=====================================\n";
	for (int i = 1; i < 2; i++) {
		cout << characters->getName() << " [HP: " << characters->getHp() << "]" << endl;
	}
	cout << "\n\n============= TURN ORDER ============\n";
	//while (character) {

	//}
	//for (int i = 1; i < _class.size() + 1; i++) {
	//	cout << "#" << i << " ["<< characters->getTeamClass()<<"] " << characters->getName();
	//}

	cout << "\nCurrent  Turn: " << "Top agility" <<endl<<endl;

}

bool Character::isAlive(){
	if (hp > 0)
		return true;
	else
		return false;
}

bool Character::canCast(Skill *skill){
	if (mp > skill->getMpCost()){
		return true;
	}
	else{
		return false;
	}
}
