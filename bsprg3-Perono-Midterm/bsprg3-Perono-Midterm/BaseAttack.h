#pragma once
#include "Skill.h"
class BaseAttack : public Skill{
public:
	BaseAttack(string name, int mpCost);
	~BaseAttack();

	void cast(Character *caster, Character *target);
};

