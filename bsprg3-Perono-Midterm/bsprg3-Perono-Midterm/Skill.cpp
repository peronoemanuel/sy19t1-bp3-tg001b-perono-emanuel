#include "Skill.h"

Skill::Skill(string name, int mpCost){
	mName = name;
	mMpCost = mpCost;
}


Skill::~Skill(){
}

void Skill::cast(Character * caster, Character * target){

}

string Skill::getName(){
	return mName;
}

int Skill::getMpCost(){
	return mMpCost;
}
