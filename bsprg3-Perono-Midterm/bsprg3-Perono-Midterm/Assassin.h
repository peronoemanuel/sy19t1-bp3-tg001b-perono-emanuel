#pragma once
#include "Classes.h"
class Assassin : public Classes
{
public:
	Assassin(string name, string characClass, string teamClass, int hp, int agi);
	~Assassin();

	void player(Character *caster, Character *target);
};

