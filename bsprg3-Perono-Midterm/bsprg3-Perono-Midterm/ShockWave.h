#pragma once
#include "Skill.h"
class ShockWave : public Skill {
public:
	ShockWave(string name, int mpCost);
	~ShockWave();

	void cast(Character *caster, Character *target);
};
