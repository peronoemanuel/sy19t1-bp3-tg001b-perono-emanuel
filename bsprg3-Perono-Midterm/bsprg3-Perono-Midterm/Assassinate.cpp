#include "Assassinate.h"

Assassinate::Assassinate(string name, int mpCost) : Skill(name, mpCost) {

}


Assassinate::~Assassinate() {

}

void Assassinate::cast(Character * caster, Character * target) {
	float damage;
	damage = caster->getDamage(caster, target) * 2.2;
	cout << caster->getName() << " uses " << mName << " against " << target->getName() << endl;
	cout << "Assassinate dealt " << damage << " damage " << endl;
	target->takeDamage(damage);
	caster->reduceMp(mMpCost);
	system("pause");
	system("cls");
}
