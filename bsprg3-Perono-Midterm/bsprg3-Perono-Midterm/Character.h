#pragma once
#include <string>
#include <vector>
#include <iostream>
using namespace std;

// Forward declare skill, do not #include "Skill.h" in Character.h
// to avoid circular references/cross-import
class Skill;
class Classes;
class Character
{
public:
	// This is a custom constructor. We use it to shorten the
	// initialization of values when creating an object
	Character(string _name, string _characClass, string _teamClass, int _hp, int _mp, int _pow, int _vit, int _agi, int _dex);
	~Character();
	int baseHp;
	int baseMp;
	int randomBasePow;
	int damage;
	//int healC

	void attack(Character *caster, Character *target);
	bool doesHit(Character *caster, Character *target);
	int getDamage(Character *caster, Character *target);
	int getRandomBasePow(Character *caster);
	bool isPlayerTurn(Character *caster, Character *target);

	// Accessor method 
	void takeDamage(int value);
	void reduceMp(int value);
	void heal(int value);


	// Getter methods
	string getName();
	string getCharacClass();
	string getTeamClass();
	int getHp();
	int getMp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	//Weapon* getWeapon();

	// These functions just help make things simple
	void printStats();
	void printTeamStats();
	bool isAlive();
	bool canCast(Skill *skill);
private:
	string name;
	string characClass;
	string teamClass;
	int hp;
	int mp;
	int pow;
	int vit;
	int agi;
	int dex;
	vector<Skill*> _skills;
	vector<Classes*> _class;
	//Weapon *mWeapon;
};

