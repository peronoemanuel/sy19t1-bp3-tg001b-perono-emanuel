#pragma once
#include "Character.h"
#include <string>
using namespace std;

class Classes{
public:
	Classes(string name, string characClass, string teamClass, int hp, int agi);
	~Classes();

	// All children of skill class can be "cast"
	// But all of them are different in the way they are cast
	// A "virtual" marker basically says "this function can be
	// overwritten"
	virtual void player(Character *caster, Character *target);

	string getName();
	string getCharacClass();
	string getTeamClass();
	int getHp();
	int getAgi();
protected:
	string _name;
	string _characClass;
	string _teamClass;
	int _hp;
	int _agi;

};

