#pragma once
#include "Character.h"
#include <string>
using namespace std;

class Skill{
public:
	Skill(string name, int mpCost);
	~Skill();

	// All children of skill class can be "cast"
	// But all of them are different in the way they are cast
	// A "virtual" marker basically says "this function can be
	// overwritten"
	virtual void cast(Character *caster, Character *target);
	//virtual void activate(Character* caster, vector<Character*> allUnits);

	string getName();
	int getMpCost();
protected:
	string mName;
	int mMpCost;
};

