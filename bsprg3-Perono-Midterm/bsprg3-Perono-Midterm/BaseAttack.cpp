#include "BaseAttack.h"

BaseAttack::BaseAttack(string name, int mpCost) : Skill(name, mpCost){

}


BaseAttack::~BaseAttack(){

}

void BaseAttack::cast(Character * caster, Character * target){

	int randGen = rand() % 100 + 1;
	int damage;
	if (randGen < 21) {
		damage = (caster->getDamage(caster, target) *  0.2) + caster->getDamage(caster, target);
		cout << "Attack Crit!" << endl;
	}
	else {
		damage = caster->getDamage(caster, target) * 1;
	}
	cout << caster->getName() << " uses " << mName << " against " << target->getName() << endl;
	cout << "Axe Smash dealt " << damage << " damage " << endl;
	target->takeDamage(damage);
	caster->reduceMp(mMpCost);
	system("pause");
	system("cls");
}
