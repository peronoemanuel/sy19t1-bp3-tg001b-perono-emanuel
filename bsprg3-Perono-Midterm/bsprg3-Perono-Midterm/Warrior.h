#pragma once
#include "Classes.h"
class Warrior : public Classes
{
public:
	Warrior(string name, string characClass, string teamClass, int hp, int agi);
	~Warrior();

	void player(Character *caster, Character *target);
};

