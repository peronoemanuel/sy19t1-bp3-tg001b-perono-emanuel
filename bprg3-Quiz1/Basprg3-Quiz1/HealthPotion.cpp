#include "HealthPotion.h"
HealthPotion::HealthPotion(string name, int count) : ItemBase(name, count) {
}
HealthPotion::~HealthPotion() {
}
void HealthPotion::roll(Player *target) {
	int heal = 30;
	cout << "Your received " << heal << " Health Potion !\n";
	target->restoreHp(heal);
}
