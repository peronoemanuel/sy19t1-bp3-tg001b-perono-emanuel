#include "Crystals.h"
Crystals::Crystals(string name, int count) : ItemBase(name, count) {
}
Crystals::~Crystals() {
}
void Crystals::roll(Player *target) {
	int crystal = 15;
	cout << "Your received " << crystal << " Crystals !\n";
	target->obtainCrystals(crystal);
}
