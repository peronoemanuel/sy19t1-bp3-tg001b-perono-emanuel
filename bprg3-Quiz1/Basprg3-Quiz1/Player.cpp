#include <iostream>
#include <string>
#include <ctime>
#include <cmath>
#include <vector>
#include <stdlib.h>  
#include "Player.h"
#include "ItemBase.h"
#include "HealthPotion.h"
#include "Bomb.h"
#include "Crystals.h"
#include "R.h"
#include "SR.h"
#include "SSR.h"

using namespace std;

Player::Player(){
	hp = 0;
	crystals = 0;
	rp = 0;
	pulls = 0;
}
Player::Player(int _hp, int _crystals, int _rp, int _pulls) {
	hp = _hp;
	crystals = _crystals;
	rp = _rp;
	pulls = _pulls;

	items.push_back(new SSR("SSR", 1));
	items.push_back(new SR("SR",1));
	items.push_back(new R("R",1));
	items.push_back(new HealthPotion("Hp",1));
	items.push_back(new Bomb("Bomb",1));
	items.push_back(new Crystals("Crystals",1));
}
Player::~Player() {
	//delete character;
}
void Player::showStatus() {
	cout << "Health: " <<hp <<endl;
	cout << "Crystals: " <<crystals <<endl;
	cout << "Rarity Points: " << rp<< endl;
	cout << "Pulls: " << pulls<<endl<<endl;

}
void Player::rollGame(Player *target) {
	int randIndex = 1;
	int randGen = rand() % 100 + 1;

	if (randGen > 0 && randGen < 16) {
		randIndex = 4; //Health Potion
	}
	else if (randGen > 15 && randGen < 31) {
		randIndex = 6; //Crystals
	}
	else if (randGen > 30 && randGen < 71) {
		randIndex = 3; //R
	}
	else if (randGen > 70 && randGen < 91) {
		randIndex = 5; //Bomb
	}
	else if (randGen > 90 && randGen < 100) {
		randIndex = 2; //SR
	}
	else if (randGen > 99 && randGen < 101)
		randIndex = 1; //SSR

	ItemBase *itemRolled = items[randIndex-1];
	cout << "You pulled " << itemRolled->geItemtName() << endl;
	itemRolled->roll(target);
}
void Player::obtainRpoints(int value) {
	rp += value;
}
void Player::takeDamage(int value) {
	if (value > 0){
		hp -= value;
		if (hp < 0)
			hp = 0;
	}
}
void Player::restoreHp(int value){
	if (value > 0){
		hp += value;
	}
}
void Player::obtainCrystals(int value) {
		crystals += value;
}
void Player::deductCrystals() {
	if (crystals != 0) {
		crystals -= 5;
	}
	else {
		cout << "You have no Crystals Left.\n";
	}
}
void Player::incrementPull() {
	pulls += 1;
}
string Player::geItemtName(){
	return this->name;
}
int Player::getCount() {
	this->count += 1;
	return this->count;
}
int Player::getHp(){
	return this->hp;
}  
int Player::getPulls(){
	return this->pulls;
}
int Player::getRp(){
	return this->rp;
}
int Player::getCrystals() {
	return this->crystals;
}
bool Player::isAlive() {
	if (hp > 0)
		return true;
	else
		return false;
}
bool Player::doesWin() {
	if (this->crystals == 0) {
		cout << "You have " << this->crystals << " Crystals!\nYou lose...\n\n";
		return false;
	}
	else if (this->rp > 99) {
		cout << "You have " << this->rp << " Rp!\nYou win!\n\n";
		return false;
	}
	else if(this->hp <1) {
		cout << "You have " << this->hp << " hp left...\nYou lose...\n\n";
		return false;
	}
	return true;
}

void Player::printGameSummary() {
	int i = 0;
	cout << "============================\n";
	cout << "HP: " << this->hp<<endl;
	cout << "Crystals: " << this->crystals << endl;
	cout << "Rarity Points: " << this->rp << endl;
	cout << "Pulls: " << this->pulls << endl;
	cout << "============================\n\n";
	cout << "Items Pulled:\n";
	cout << "----------------------------\n";
	while (i!= 5) {
		ItemBase *itemRolled = items[i];
		cout << itemRolled->geItemtName() << " x"<<itemRolled->getCount()<<endl;
		i++;
	}

}


