#include "Bomb.h"
Bomb::Bomb(string name, int count) : ItemBase(name, count){

}
Bomb::~Bomb(){

}
void Bomb::roll(Player *target){
	int damage = 25;
	cout << "Your received " << damage << " damage !\n";
	target->takeDamage(damage);
}
