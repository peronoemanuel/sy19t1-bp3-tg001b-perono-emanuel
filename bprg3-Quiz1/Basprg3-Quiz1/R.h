#pragma once
#include <iostream>
#include "ItemBase.h"
#include "Player.h"


class R : public ItemBase{
public:
	R(string name, int count);
	~R();
	void roll(Player *target);
};

