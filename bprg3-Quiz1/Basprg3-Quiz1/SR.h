#pragma once
#include <iostream>
#include "ItemBase.h"
#include "Player.h"


class SR : public ItemBase {
public:
	SR(string name, int count);
	~SR();
	void roll(Player *target);
};

