#pragma once
#include <iostream>
#include <string>
#include "ItemBase.h"
#include <vector>
#include "Player.h"

class HealthPotion : public ItemBase{
public:
	HealthPotion(string name, int count);
	~HealthPotion();
	void roll(Player *target);
};

