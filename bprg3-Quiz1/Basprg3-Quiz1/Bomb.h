#pragma once
//#include "ItemBase.h"
#include <iostream>
#include "Player.h"
#include "ItemBase.h"

class Bomb : public ItemBase{
public:
	Bomb(string name, int count);
	~Bomb();
	void roll(Player *target);
};

