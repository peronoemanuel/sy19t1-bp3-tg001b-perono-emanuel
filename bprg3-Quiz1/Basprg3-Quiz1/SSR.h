#pragma once
#include <iostream>
#include "ItemBase.h"
#include "Player.h"


class SSR : public ItemBase {
public:
	SSR(string name, int count);
	~SSR();

	void roll(Player *target);
};

#pragma once
