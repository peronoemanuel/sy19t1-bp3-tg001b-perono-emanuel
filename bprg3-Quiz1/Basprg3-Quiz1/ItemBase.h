#pragma once
//#include "Player.h"
#include <string>
#include <iostream>
using namespace std;

class Player;
class ItemBase{
public:
	ItemBase(string name, int count);
	~ItemBase();
	virtual void roll(Player *target);

	string geItemtName();
	int getCount();
	//int getMpCost();

protected:
	string _name;
	int _count;
	//int _mpCost;
};

