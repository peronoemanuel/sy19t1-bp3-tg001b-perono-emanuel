#pragma once
//#include "ItemBase.h"
#include <iostream>
#include "Player.h"
#include "ItemBase.h"

class Crystals : public ItemBase {
public:
	Crystals(string name, int count);
	~Crystals();
	void roll(Player *target);
};

