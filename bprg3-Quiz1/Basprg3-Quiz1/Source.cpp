#include <iostream>
#include <string>
#include <ctime>
#include <cmath>
#include <vector>
#include "Player.h"
#include "ItemBase.h"
using namespace std;
int main() {
	srand(time(NULL));
	Player *player1 = new Player(100, 100, 0, 0);
	while (player1->doesWin()){
		player1->showStatus();
		player1->rollGame(player1);
		system("pause");
		system("cls");
		player1->deductCrystals();
		player1->incrementPull();
	}
	player1->printGameSummary();
	system("pause");
	return 0;
}