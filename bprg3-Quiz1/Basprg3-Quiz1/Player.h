#pragma once
#include <iostream>
#include <string>
#include "Bomb.h"
#include "HealthPotion.h"
#include <vector>
#include "ItemBase.h"
#include "Crystals.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"

using namespace std;

//class ItemBase;
class Player {
public:
	Player();
	~Player();
	Player(int _hp, int _crystals, int _rp, int _pulls);

	void showStatus();
	void rollGame(Player *target);
	// Accessor method
	void takeDamage(int value);
	void restoreHp(int value);
	void obtainCrystals(int value);
	void obtainRpoints(int value);
	bool isAlive();
	bool doesWin();

	void deductCrystals();
	void incrementPull();

	int getHp();
	int getPulls();
	int getRp();
	int getCrystals();
	string geItemtName();
	int getCount();

	void printGameSummary();

private:

	vector<ItemBase*> items;
	string name;
	int hp;
	int crystals;
	int rp;
	int pulls;
	int count; 
	



};
